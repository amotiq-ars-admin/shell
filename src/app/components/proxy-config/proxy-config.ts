import { IProxyConfig } from './proxy-config.component';

export const proxyConfig: IProxyConfig[] =
  [
    {
      backendName: 'BusinessUnit',
      json: '  "/api/BusinessUnit": {\n' +
        '    "target": "http://localhost:8010",\n' +
        '    "secure": false,\n' +
        '    "logLevel": "debug",\n' +
        '    "changeOrigin": true\n' +
        '  }'
    },
    {
      backendName: 'StorageLocation',
      json: '  "/api/StorageLocation": {\n' +
        '    "target": "http://localhost:8018",\n' +
        '    "secure": false,\n' +
        '    "logLevel": "debug",\n' +
        '    "changeOrigin": true\n' +
        '  }'
    },
    {
      backendName: 'FactoryCalendarService',
      json: '  "/api/FactoryCalendarService": {\n' +
        '    "target": "http://localhost:8014",\n' +
        '    "secure": false,\n' +
        '    "logLevel": "debug",\n' +
        '    "changeOrigin": true\n' +
        '  }'
    },
    {
      backendName: 'BusinessPartnerService',
      json: '  "/api/BusinessPartnerService": {\n' +
        '    "target": "http://localhost:8008",\n' +
        '    "secure": false,\n' +
        '    "logLevel": "debug",\n' +
        '    "changeOrigin": true\n' +
        '  }'
    },
    {
      backendName: 'BaseData',
      json: '  "/api/BaseData": {\n' +
        '    "target": "http://localhost:8004",\n' +
        '    "secure": false,\n' +
        '    "logLevel": "debug",\n' +
        '    "changeOrigin": true\n' +
        '  }'
    },
    {
      backendName: 'LogHandler',
      json: '  "/api/LogHandler": {\n' +
        '    "target": "http://localhost:9014",\n' +
        '    "secure": false,\n' +
        '    "logLevel": "debug",\n' +
        '    "changeOrigin": true\n' +
        '  }'
    },
    {
      backendName: 'ConfigProvider',
      json: '  "/api/ConfigProvider": {\n' +
        '    "target": "http://localhost:9004",\n' +
        '    "secure": false,\n' +
        '    "logLevel": "debug",\n' +
        '    "changeOrigin": true\n' +
        '  }'
    },
    {
      backendName: 'MaterialBaseData',
      json: '  "/api/MaterialBaseData": {\n' +
        '    "target": "http://localhost:8016",\n' +
        '    "secure": false,\n' +
        '    "logLevel": "debug",\n' +
        '    "changeOrigin": true\n' +
        '  }'
    },
    {
      backendName: 'MaterialMovementJournal',
      json: '  "/api/MaterialMovementJournal": {\n' +
        '    "target": "http://localhost:8020",\n' +
        '    "secure": false,\n' +
        '    "logLevel": "debug",\n' +
        '    "changeOrigin": true\n' +
        '  }'
    },
    {
      backendName: 'SCRelationService',
      json: '  "/api/SCRelationService": {\n' +
        '    "target": "http://localhost:8012",\n' +
        '    "secure": false,\n' +
        '    "logLevel": "debug",\n' +
        '    "changeOrigin": true\n' +
        '  }'
    },
    {
      backendName: 'UserManager',
      json: '  "/api/UserManager": {\n' +
        '    "target": "http://localhost:9026",\n' +
        '    "secure": false,\n' +
        '    "logLevel": "debug",\n' +
        '    "changeOrigin": true\n' +
        '  }'
    },
    {
      backendName: 'EinteilungsService',
      json: '  "/api/EinteilungsService": {\n' +
        '    "target": "http://localhost:8006",\n' +
        '    "secure": false,\n' +
        '    "logLevel": "debug",\n' +
        '    "changeOrigin": true\n' +
        '  }'
    },
    {
      backendName: 'LanguageProvider',
      json: '  "/api/LanguageProvider": {\n' +
        '    "target": "http://localhost:9024",\n' +
        '    "secure": false,\n' +
        '    "logLevel": "debug",\n' +
        '    "changeOrigin": true\n' +
        '  }'
    },
    {
      backendName: 'WeService',
      json: '  "/api/WeService": {\n' +
        '    "target": "http://localhost:8024",\n' +
        '    "secure": false,\n' +
        '    "logLevel": "debug",\n' +
        '    "changeOrigin": true\n' +
        '  }'
    },
    {
      backendName: 'ExportService',
      json: '  "/api/ExportService": {\n' +
        '    "target": "http://localhost:8022",\n' +
        '    "secure": false,\n' +
        '    "logLevel": "debug",\n' +
        '    "changeOrigin": true\n' +
        '  }'
    },
    {
      backendName: 'SignalRHub',
      json: '  "/api/SignalRHub": {\n' +
        '    "target": "http://localhost:9012",\n' +
        '    "secure": false,\n' +
        '    "logLevel": "debug",\n' +
        '    "changeOrigin": true\n' +
        '  }'
    },
    {
      backendName: 'SignalR-GuiSignal',
      json: '  "/GuiSignal": {\n' +
        '    "target": "http://localhost:9012",\n' +
        '    "secure": false,\n' +
        '    "logLevel": "debug",\n' +
        '    "changeOrigin": true\n' +
        '  }'
    },
    {
      backendName: 'SignalR-IQaRSFrontend',
      json: '  "/IQaRSFrontend": {\n' +
        '    "target": "http://localhost:9012",\n' +
        '    "secure": false,\n' +
        '    "logLevel": "debug",\n' +
        '    "changeOrigin": true\n' +
        '  }'
    }
  ];
