import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { proxyConfig } from './proxy-config';

@Component({
  selector: 'app-proxy-config',
  templateUrl: './proxy-config.component.html',
  styleUrls: ['./proxy-config.component.css']
})
export class ProxyConfigComponent {
  public availableProxies: IProxyConfig[] = [];
  public proxySelect: FormGroup = new FormGroup({});

  constructor() {
    this.availableProxies = this.getProxies();
    this.availableProxies.forEach(proxy => {
      this.proxySelect.addControl(proxy.backendName, new FormControl());
    });
  }

  public getProxies(): IProxyConfig[] {
    // Sort by name
    return proxyConfig.sort((proxyA, proxyB) => {
      return proxyA.backendName.localeCompare(proxyB.backendName);
    });
  }

  public buildProxyJSON(): void {
    let json = '{\n';
    this.availableProxies.forEach(proxy => {
      if (this.proxySelect.get(proxy.backendName)?.value) {
        json = `${json + proxy.json},\n`;
      }
    });
    json = `${json}"/api/*": {\n` +
      `    "target": "http://reverseproxy.ci.iqars.de:8080",\n` +
      `    "secure": false,\n` +
      `    "logLevel": "debug",\n` +
      `    "changeOrigin": true\n` +
      `  }\n` +
      `}`;

    const file = new File([json as BlobPart], 'proxy.conf.json', { type: 'application/json' });
    // Create anchor for download
    const a = document.createElement('a');
    a.href = URL.createObjectURL(file);
    a.download = file.name;
    a.click();
  }

}

export interface IProxyConfig {
  backendName: string;
  json: string;
}
