import { AmotiqTranslateService } from '@amotiq/amotiq-base';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.scss'],
})
export class UserSettingsComponent implements OnInit, OnDestroy {
  public availableLanguages: string[];
  public languageSelect: FormGroup;
  private languageSelectChangeSubscription: Subscription;
  public time: Date;

  constructor(private fb: FormBuilder, private translate: AmotiqTranslateService) {
    this.time = new Date();
    this.languageSelect = this.fb.group({
      languageRadioGroup: [this.translate.getCurrentLanguage()],
    });
    // tslint:disable-next-line
    this.languageSelectChangeSubscription = this.languageSelect.valueChanges.subscribe((lang: { [formGroup: string]: string }) => {
      this.setNewLanguage(lang.languageRadioGroup);
    });
    this.availableLanguages = this.translate.getAvailableLanguagesFromTranslate();
  }

  public ngOnInit(): void {
    setInterval(() => {
      this.time = new Date();
    }, 1000);
  }

  public ngOnDestroy(): void {
    this.languageSelectChangeSubscription.unsubscribe();
  }

  private setNewLanguage(selectedLanguage: string): void {
    this.translate.setLanguage(selectedLanguage);
  }
}
