import { amotiqPasswordValidator, HttpcommService, NotificationService } from '@amotiq/amotiq-base';
import { UserService } from '@amotiq/auth-lib';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MdbModalConfig, MdbModalRef, MdbModalService } from 'mdb-angular-ui-kit/modal';
import { IOwnPasswordDto } from '../../interfaces/iown-password-dto';
import { PasswordChangedSuccessComponent } from '../../modals/password-changed-success/password-changed-success.component';
import { accountSettingsEndpoints } from '../../shared/endpoints';

@Component({
  selector: 'app-account-settings',
  templateUrl: './account-settings.component.html',
  styleUrls: ['./account-settings.component.scss']
})
export class AccountSettingsComponent implements OnInit {
  public time: Date;
  public changePasswordForm: FormGroup;

  constructor(private fb: FormBuilder,
              private http: HttpcommService,
              private notification: NotificationService,
              private mdbModalService: MdbModalService,
              private userService: UserService) {
    this.time = new Date();

    this.changePasswordForm = this.fb.group({
      oldPassword: [{ value: '' }, Validators.required],
      newPassword: [{ value: '' }, [Validators.required, Validators.minLength(8), amotiqPasswordValidator()]],
      newPasswordConfirmation: [{ value: '' }, [Validators.required, Validators.minLength(8)]]
    });
  }

  public ngOnInit(): void {
    setInterval(() => {
      this.time = new Date();
    }, 1000);

    this.newPasswordConfirmation?.addValidators(amotiqPasswordValidator(this.newPassword));
  }

  public getErrorNewPassword(): string {
    return this.getErrorPasswordFields(this.newPassword);
  }

  public getErrorNewPasswordConfirmation(): string {
    return this.getErrorPasswordFields(this.newPasswordConfirmation);
  }

  private getErrorPasswordFields(control: AbstractControl | null): string {
    if (control && control.invalid && control.errors) {
      const errors = control.errors;
      if (errors.required) {
        return 'shared.validations.must-be-filled';
      }
      if (errors.missingUppercase) {
        return 'shared.validations.needs-upper-case';
      }
      if (errors.missingLowercase) {
        return 'shared.validations.needs-lower-case';
      }
      if (errors.minlength) {
        return 'shared.validation.min-length-8';
      }
      if (errors.passwordNotMatching) {
        return 'projects.iqars.components.account-settings.changepassword.password-not-matching';
      }
      return 'UNKNOWN ERROR';
    } else {
      return '';
    }
  }

  public submit(): void {
    this.changePasswordForm.markAsDirty();
    this.changePasswordForm.markAsTouched();
    if (this.changePasswordForm.valid) {
      this.putChangedPassword();
    }
  }

  private putChangedPassword(): void {
    const changedPasswordDto: IOwnPasswordDto = {
      oldPassword: this.oldPassword?.value,
      newPassword: this.newPassword?.value,
      confirmPassword: this.newPasswordConfirmation?.value,
      username: this.userService.getUserName(),
      statusMessage: ''
    };

    this.http.put<IOwnPasswordDto>(accountSettingsEndpoints.updatePassword, changedPasswordDto).subscribe(() => {
        this.openSuccessModal().onClose.subscribe(() => {
          this.userService.logout(window.origin);
        });
      },
      (error) => {
        console.error(error);
        this.notification.error('projects.iqars.components.account-settings.notifications.password-change-failed');
      });
  }

  private openSuccessModal(): MdbModalRef<PasswordChangedSuccessComponent> {
    const modalConfig: MdbModalConfig = {
      animation: false,
      modalClass: 'modal-dialog-centered modal-xl',
      backdrop: true,
      ignoreBackdropClick: true
    };

    return this.mdbModalService.open(PasswordChangedSuccessComponent, modalConfig);
  }

  public get oldPassword(): AbstractControl | null {
    return this.changePasswordForm.get('oldPassword');
  }

  public get newPassword(): AbstractControl | null {
    return this.changePasswordForm.get('newPassword');
  }

  public get newPasswordConfirmation(): AbstractControl | null {
    return this.changePasswordForm.get('newPasswordConfirmation');
  }

}
