export interface IOwnPasswordDto {
  oldPassword: string;
  newPassword: string;
  confirmPassword: string;
  statusMessage: string;
  username: string;
}
