import {
  AcceptChangesDialogComponent,
  AmotiqBaseModule,
  AmotiqTableToolbarComponent,
  AmotiqTranslateService,
  BaseGridComponent,
  BreadcrumbComponent,
  HttpcommService,
  HTTPInterceptorInterceptor,
  NotificationService
} from '@amotiq/amotiq-base';
import { AuthModule, authModuleConfig, AuthService, AuthStorage, getAuthConfig, UserService } from '@amotiq/auth-lib';
import { BaseDataApiModule, Configuration } from '@amotiq/basedata-api';
import { BusinessPartnerApiModule } from '@amotiq/businesspartner-api';
import { BusinessUnitApiModule } from '@amotiq/businessunit-api';
import { EdiHandlerApiModule } from '@amotiq/edihandler-api';
import { EinteilungApiModule } from '@amotiq/einteilung-api';
import { ExportApiModule } from '@amotiq/export-api';
import { MaterialBaseDataApiModule } from '@amotiq/materialbasedata-api';
import { MaterialMovementApiModule } from '@amotiq/materialmovement-api';
import { MrpApiModule } from '@amotiq/mrp-api';
import { SCRelationApiModule } from '@amotiq/screlation-api';
import { ShipmentAndDevliveryApiModule } from '@amotiq/shipmentanddelivery-api';
import { StorageLocationApiModule } from '@amotiq/storagelocation-api';
import { WeServiceApiModule } from '@amotiq/weservice-api';
import { LOCATION_INITIALIZED } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { APP_INITIALIZER, Injector, LOCALE_ID, NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { MissingTranslationHandler, TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AuthConfig, OAuthModuleConfig } from 'angular-oauth2-oidc';
import { MdbChartModule } from 'mdb-angular-ui-kit/charts';
import { MdbCheckboxModule } from 'mdb-angular-ui-kit/checkbox';
import { MdbCollapseModule } from 'mdb-angular-ui-kit/collapse';
import { MdbDatepickerModule } from 'mdb-angular-ui-kit/datepicker';
import { MdbDropdownModule } from 'mdb-angular-ui-kit/dropdown';
import { MdbFormsModule } from 'mdb-angular-ui-kit/forms';
import { MdbModalModule } from 'mdb-angular-ui-kit/modal';
import { MdbNotificationModule } from 'mdb-angular-ui-kit/notification';
import { MdbPopoverModule } from 'mdb-angular-ui-kit/popover';
import { MdbRadioModule } from 'mdb-angular-ui-kit/radio';
import { MdbRangeModule } from 'mdb-angular-ui-kit/range';
import { MdbRatingModule } from 'mdb-angular-ui-kit/rating';
import { MdbRippleModule } from 'mdb-angular-ui-kit/ripple';
import { MdbScrollbarModule } from 'mdb-angular-ui-kit/scrollbar';
import { MdbScrollspyModule } from 'mdb-angular-ui-kit/scrollspy';
import { MdbSelectModule } from 'mdb-angular-ui-kit/select';
import { MdbSidenavModule } from 'mdb-angular-ui-kit/sidenav';
import { MdbStepperModule } from 'mdb-angular-ui-kit/stepper';
import { MdbStickyModule } from 'mdb-angular-ui-kit/sticky';
import { MdbTableModule } from 'mdb-angular-ui-kit/table';
import { MdbTabsModule } from 'mdb-angular-ui-kit/tabs';
import { MdbTimepickerModule } from 'mdb-angular-ui-kit/timepicker';
import { MdbTooltipModule } from 'mdb-angular-ui-kit/tooltip';
import { MdbValidationModule } from 'mdb-angular-ui-kit/validation';
import { LoggerModule, NgxLoggerLevel } from 'ngx-logger';
import { AppComponent } from './app.component';
import { APP_ROUTES } from './app.routes';
import { AccountSettingsComponent } from './components/account-settings/account-settings.component';
import { ProxyConfigComponent } from './components/proxy-config/proxy-config.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { UserSettingsComponent } from './components/user-settings/user-settings.component';
import { HomeComponent } from './home/home.component';
import { PasswordChangedSuccessComponent } from './modals/password-changed-success/password-changed-success.component';
import { endpointsAdditional } from './shared/endpoints';

export const httpLoaderFactory = (http: HttpClient): TranslateHttpLoader => {
  // return new TranslateHttpLoader(http, `/api/LanguageProvider/v1/LangProvider/GetLanguageKeys?language=`, '');
  return new TranslateHttpLoader(http);
};

export const storageFactory = (): AuthStorage => {
  return new AuthStorage();
};

export function translateInit(translate: AmotiqTranslateService, injector: Injector) {
  return (): Promise<any> => new Promise<any>((resolve: any) => {
    const locationInit = injector.get(LOCATION_INITIALIZED, Promise.resolve(null));
    locationInit.then(() => {
      translate.getAvailableLanguages().then((langs) => {
        translate.addLanguages(langs);
        translate.setLanguage(translate.getLanguageFromLocalStorage() || translate.getBrowserLanguage() || 'en').subscribe(() => {
          resolve(null);
        });
      });
    });
    // translate.addLanguages(lang);
    // await translate.setLanguage(translate.getLanguageFromLocalStorage() || translate.getBrowserLanguage() || 'en');
    // return Promise.resolve();
  });
}

export function createApiConfiguration(): Configuration {
  return {
    basePath: window.origin,
    lookupCredential(key: string): string | undefined {
      return undefined;
    },
    selectHeaderAccept(accepts: string[]): string | undefined {
      const acceptsFiltered = accepts.filter((accept) => accept !== '*/*');
      if (acceptsFiltered.length > 0) {
        return acceptsFiltered.sort().toString();
      } else {
        return 'application/json';
      }
    },
    selectHeaderContentType(contentTypes: string[]): string | undefined {
      // We're always posting a JSON
      if (!contentTypes.includes('application/json')) {
        console.error('Endpoint doesn\'t accept JSON');
        return undefined;
      } else {
        return 'application/json';
      }
    },
    isJsonMime(mime: string): boolean {
      const jsonMime = new RegExp('^(application\/json|[^;/ \t]+\/[^;/ \t]+[+]json)[ \t]*(;.*)?$', 'i');
      return mime !== null && (jsonMime.test(mime) || mime.toLowerCase() === 'application/json-patch+json');
    },
    credentials: {}
  };
}

@NgModule({
  declarations: [AppComponent, HomeComponent, UserSettingsComponent, SidebarComponent, ProxyConfigComponent, AccountSettingsComponent, PasswordChangedSuccessComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AuthModule,
    ReactiveFormsModule,
    MdbChartModule,
    MdbCheckboxModule,
    MdbCollapseModule,
    MdbDatepickerModule,
    MdbDropdownModule,
    MdbFormsModule,
    MdbModalModule,
    MdbNotificationModule,
    MdbPopoverModule,
    MdbRadioModule,
    MdbRangeModule,
    MdbRatingModule,
    MdbRippleModule,
    MdbScrollbarModule,
    MdbScrollspyModule,
    MdbSelectModule,
    MdbSidenavModule,
    MdbStepperModule,
    MdbStickyModule,
    MdbTableModule,
    MdbTabsModule,
    MdbTimepickerModule,
    MdbTooltipModule,
    MdbValidationModule,
    AmotiqBaseModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useClass: AmotiqTranslateService,
        // useFactory: httpLoaderFactory,
        deps: [HttpcommService]
        // deps: [HttpClient],
      },
      missingTranslationHandler: {
        provide: MissingTranslationHandler,
        useClass: AmotiqTranslateService,
        deps: [HttpcommService]
      },
      isolate: false
    }),
    LoggerModule.forRoot({
      serverLoggingUrl: endpointsAdditional.Logger,
      serverLogLevel: NgxLoggerLevel.TRACE,
      level: NgxLoggerLevel.TRACE,
      colorScheme: ['cyan', 'green', 'gray', 'white', 'yellow', 'red', 'red']
    }),
    RouterModule.forRoot(APP_ROUTES),
    BaseDataApiModule.forRoot(() => {
      return createApiConfiguration();
    }),
    BusinessPartnerApiModule.forRoot(() => {
      return createApiConfiguration();
    }),
    BusinessUnitApiModule.forRoot(() => {
      return createApiConfiguration();
    }),
    EdiHandlerApiModule.forRoot(() => {
      return createApiConfiguration();
    }),
    EinteilungApiModule.forRoot(() => {
      return createApiConfiguration();
    }),
    ExportApiModule.forRoot(() => {
      return createApiConfiguration();
    }),
    MaterialBaseDataApiModule.forRoot(() => {
      return createApiConfiguration();
    }),
    MaterialMovementApiModule.forRoot(() => {
      return createApiConfiguration();
    }),
    MrpApiModule.forRoot(() => {
      return createApiConfiguration();
    }),
    SCRelationApiModule.forRoot(() => {
      return createApiConfiguration();
    }),
    ShipmentAndDevliveryApiModule.forRoot(() => {
      return createApiConfiguration();
    }),
    StorageLocationApiModule.forRoot(() => {
      return createApiConfiguration();
    }),
    WeServiceApiModule.forRoot(() => {
      return createApiConfiguration();
    })
  ],
  entryComponents: [AcceptChangesDialogComponent, BaseGridComponent, BreadcrumbComponent, AmotiqTableToolbarComponent],
  providers: [
    { provide: AuthConfig, useFactory: (): AuthConfig => getAuthConfig(window.origin) },
    { provide: OAuthModuleConfig, useValue: authModuleConfig },
    {
      provide: APP_INITIALIZER,
      useFactory: (authService: AuthService) => (): Promise<void> => authService.runInitialLoginSequence(),
      deps: [AuthService],
      multi: true
    },
    {
      provide: APP_INITIALIZER,
      useFactory: translateInit,
      deps: [AmotiqTranslateService, Injector],
      multi: true
    },
    { provide: LOCALE_ID, useValue: navigator.language },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HTTPInterceptorInterceptor,
      multi: true
    },
    UserService,
    AmotiqTranslateService,
    HttpClient,
    NotificationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
