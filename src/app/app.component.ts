import { AmotiqTranslateService } from '@amotiq/amotiq-base';
import { UserService } from '@amotiq/auth-lib';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { buildRoutes } from '../menu-utils';
import { LookupService } from './microfrontends/lookup.service';
import { Microfrontend } from './microfrontends/microfrontend';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  public title = 'IQaRS';
  public groupedMfs: any[];
  public showSideNavMenu: boolean;

  constructor(
    private router: Router,
    private lookupService: LookupService,
    private translate: AmotiqTranslateService,
    private userService: UserService
  ) {
    let mf: Microfrontend[] = this.lookupService.lookup();
    this.showSideNavMenu = true;
    this.groupedMfs = [];

    const routes = buildRoutes(mf);
    this.router.config.push(...routes);

    // Filter MFEs to only show the ones you're allowed to see.
    mf = mf.filter((mainEntry) => this.hasRole(mainEntry.requireRole));
    mf.forEach(item => {
      item.components = item.components.filter((subItem) => this.hasRole(subItem.requireRole));
    });

    this.generateSidenavItems(mf);
  }

  private generateSidenavItems(mfArray: Microfrontend[]): void {
    this.groupedMfs = mfArray.reduce((acc: any[], mf) => {
      const mfKey = mf.groupName;
      const tempMfArray = acc.findIndex((item: { groupName: string, items: Microfrontend[] }) => item.groupName === mfKey);
      const tempObj = {
        groupName: mfKey,
        displayName: mf.groupNameTranslationString,
        icon: mf.iconClass,
        requireRole: mf.requireRole,
        items: [mf] as Microfrontend[]
      };

      if (tempMfArray < 0) {
        acc.push(tempObj);
      } else {
        acc[tempMfArray].items.push(mf);
      }

      return acc;

    }, []) as unknown as any[];
  }

  public onClickLoginLogout(): void {
    if (this.userService.isAuthenticated()) {
      this.userService.logout(window.origin);
    } else {
      this.userService.login(window.origin);
    }
  }

  public getUserNameAndTenant(): string {
    if (this.userService.isAuthenticated()) {
      return `${this.userService.getUserName()} (${this.userService.getTenant()})`;
    } else {
      return this.translate.translateInstant('projects.iqars.nav.notloggedin');
    }
  }

  public getLoginButtonState(): string {
    if (this.userService.isAuthenticated()) {
      return 'projects.iqars.nav.usermenu.logout';
    } else {
      return 'projects.iqars.nav.usermenu.login';
    }
  }

  public userIsLoggedIn(): boolean {
    return this.userService.isAuthenticated();
  }

  public runningLocal(): boolean {
    return window.origin.includes('localhost');
  }

  public hasRole(role: string | string[] | null): boolean {
    return this.userService.hasRole(role);
  }
}
