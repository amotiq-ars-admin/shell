export const endpointsAdditional = {
  Logger: '/api/LogHandler/v1/LogHandler/CreateLogEntry'
};

export const accountSettingsEndpoints = {
  updatePassword: '/api/UserManager/v1/Account/account/password'
};
