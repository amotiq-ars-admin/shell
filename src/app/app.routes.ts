import { AuthGuardGuard } from '@amotiq/auth-lib';
import { Routes } from '@angular/router';
import { UserSettingsComponent } from './components/user-settings/user-settings.component';
import { HomeComponent } from './home/home.component';
import { ProxyConfigComponent } from './components/proxy-config/proxy-config.component';
import { AccountSettingsComponent } from './components/account-settings/account-settings.component';

export const APP_ROUTES: Routes = [
  {
    path: '',
    component: HomeComponent,
    pathMatch: 'full',
    data: {
      breadcrumb: 'Home',
    },
  },
  {
    path: 'usersettings',
    component: UserSettingsComponent,
    pathMatch: 'full',
    canActivate: [AuthGuardGuard],
    data: {
      breadcrumb: 'Home',
    },
  },
  {
    path: 'accountsettings',
    component: AccountSettingsComponent,
    pathMatch: 'full',
    canActivate: [AuthGuardGuard],
    data: {
      breadcrumb: 'Home',
    },
  },
  {
    path: 'proxyconfig',
    component: ProxyConfigComponent,
    pathMatch: 'full',
    canActivate: [AuthGuardGuard],
    data: {
      breadcrumb: 'Home',
    },
  },
  // {
  //   path: 'materials',
  //   loadChildren: () => loadRemoteModule({
  //       remoteEntry: 'http://localhost:3000/remoteEntry.js',
  //       remoteName: 'materials',
  //       exposedModule: 'Module'
  //     })
  //     .then(m => m.MaterialsModule)
  // }
];

// const routes: Routes = [];

// @NgModule({
//   imports: [RouterModule.forRoot(routes)],
//   exports: [RouterModule]
// })
// export class AppRoutingModule { }
