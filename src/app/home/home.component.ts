import { AuthService, UserService } from '@amotiq/auth-lib';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {
  constructor(private authService: AuthService) {}

  public ngOnInit(): void {
    // this.authService.isDoneLoading$.subscribe((doneLoading: boolean) => {
    //   console.debug('doneLoading');
    //   if (doneLoading) {
    //     this.authService.isAuthenticated$.subscribe((authenticated: boolean) => {
    //       console.debug(authenticated);
    //       if (!authenticated) {
    //         this.authService.login(window.origin);
    //       }
    //     });
    //   }
    // });
  }
}
