import { Injectable } from '@angular/core';
import { Microfrontend } from './microfrontend';

/**
 * The order of declared microfrontends DOES MATTER!
 *
 * Based on this structure, whole sidenav menu will be build.
 *
 * isGroup: true/false - if microfrontend belongs to group, if so the the
 * groupName and groupNameTranslationString must be specified with group name
 * and translation string
 *
 * flatComponents: true/false (NOT FULLY IMPLEMENTED YET) - should microfrontend components be flatten -
 * microfrontend components will be shown at the same level (1 level in sidenav
 * tree), otherwise microfrontend will be shown as subgroup in sidenav
 *
 */
@Injectable({ providedIn: 'root' })
export class LookupService {
  public lookup(): Microfrontend[] {
    return [
      // LVS
      {
        // For Loading
        remoteEntry: `${window.origin}/lvs-discovery/remoteEntry.js`,
        remoteName: 'lvs',
        exposedModule: './Module',

        // For Routing
        displayName: 'projects.iqars.nav.sidenavmenu.lvs',
        routePath: 'lvs',
        groupRoute: '',
        isGroup: true,
        flatComponents: true,
        ngModuleName: 'LVSModule',
        iconClass: 'fas fas fa-box-open',
        groupName: 'LVS',
        groupNameTranslationString: 'projects.iqars.nav.sidenavmenu.lvs',
        requireRole: null,
        components: [
          {
            displayName: 'projects.lvs.components.goods-receipt.title-nav',
            routePath: 'lvs/goods-receipt',
            requireRole: null
          },
          {
            displayName: 'projects.lvs.components.stock-movement.title-nav',
            routePath: 'lvs/stockmovement',
            requireRole: null
          },
          {
            displayName: 'projects.lvs.components.pack.title-nav',
            routePath: 'lvs/pack',
            requireRole: null
          },
          {
            displayName: 'projects.lvs.components.movement-journal.title-nav',
            routePath: 'lvs/movement-journal',
            requireRole: null
          },
          {
            displayName: 'projects.lvs.components.inventory-overview.title-nav',
            routePath: 'lvs/inventory-overview',
            requireRole: null
          },
          {
            displayName: 'projects.lvs.components.record-inventory.title-nav',
            routePath: 'lvs/record-inventory',
            requireRole: null
          }
        ]
      },
      // Dispo
      {
        // For Loading
        remoteEntry: `${window.origin}/dispo-discovery/remoteEntry.js`,
        remoteName: 'dispo',
        exposedModule: './Module',

        // For Routing
        displayName: 'projects.iqars.nav.sidenavmenu.dispo',
        routePath: 'dispo',
        groupRoute: '',
        isGroup: true,
        flatComponents: true,
        ngModuleName: 'DispoModule',
        iconClass: 'fas fas fa-box-open',
        groupName: 'Dispo',
        groupNameTranslationString: 'projects.iqars.nav.sidenavmenu.dispo',
        requireRole: null,
        components: [
          {
            displayName: 'projects.dispo.components.mrp-monitor.title-nav',
            routePath: 'dispo/mrp-monitor',
            requireRole: null
          },
          {
            displayName: 'projects.dispo.components.dispo.title',
            routePath: 'dispo/overview',
            requireRole: null
          }
        ]
      },
      // Einteilungsmonitor
      {
        // For Loading
        // remoteEntry: 'http://iqarsfrontend-einteilungsmonitor.dev.iqars.de:8080/remoteEntry.js',
        remoteEntry: `${window.origin}/einteilungsmonitor-discovery/remoteEntry.js`,
        remoteName: 'einteilungsmonitor',
        exposedModule: './Module',

        // For Routing
        displayName: 'projects.iqars.nav.sidenavmenu.classificationmonitor',
        routePath: 'einteilungsmonitor',
        groupRoute: '',
        isGroup: false,
        flatComponents: false,
        ngModuleName: 'EinteilungsmonitorModule',
        iconClass: 'fas fas fa-table',
        groupName: 'Dispo',
        groupNameTranslationString: 'projects.materialstamm.dispo',
        components: [],
        requireRole: null
      },
      // Materialstamm
      {
        // For Loading
        // remoteEntry: 'http://iqarsfrontend-materialstamm.dev.iqars.de:8080/remoteEntry.js',
        remoteEntry: `${window.origin}/materialstamm-discovery/remoteEntry.js`,
        remoteName: 'materialstamm',
        exposedModule: './Module',

        // For Routing
        displayName: 'projects.materialstamm.sidenav-group-title',
        routePath: 'stammdaten/materialstamm',
        groupRoute: 'stammdaten',
        ngModuleName: 'MaterialstammModule',
        iconClass: 'fas fa-user',
        groupName: 'Stammdaten',
        isGroup: true,
        flatComponents: false,
        groupNameTranslationString: 'projects.iqars.nav.sidenavmenu.basedata',
        components: [],
        requireRole: null
      },
      // Businesspartner
      {
        // For Loading
        // remoteEntry: 'http://iqarsfrontend-businesspartner.dev.iqars.de:8080/remoteEntry.js',
        remoteEntry: `${window.origin}/businesspartner-discovery/remoteEntry.js`,
        remoteName: 'businesspartner',
        exposedModule: './Module',

        // For Routing
        displayName: 'projects.businesspartner.sidenav-group-title',
        groupRoute: 'stammdaten',
        isGroup: false,
        flatComponents: false,
        routePath: 'stammdaten/businesspartner',
        ngModuleName: 'BusinessPartnerModule',
        iconClass: 'fas fa-handshake',
        groupName: 'Stammdaten',
        groupNameTranslationString: 'projects.iqars.nav.sidenavmenu.basedata',
        components: [],
        requireRole: null
      },
      // Businessunit
      {
        // For Loading
        // remoteEntry: 'http://iqarsfrontend-businessunit.dev.iqars.de:8080/remoteEntry.js',
        remoteEntry: `${window.origin}/businessunit-discovery/remoteEntry.js`,
        remoteName: 'businessunit',
        exposedModule: './Module',

        // For Routing
        displayName: 'projects.business-unit.sidenav-group-title',
        routePath: 'stammdaten/businessunit',
        groupRoute: 'stammdaten',
        isGroup: false,
        flatComponents: false,
        ngModuleName: 'BusinessUnitModule',
        iconClass: 'fas fa-briefcase',
        groupName: 'Stammdaten',
        groupNameTranslationString: 'projects.iqars.nav.sidenavmenu.basedata',
        components: [],
        requireRole: null
      },
      // Sc-Relation
      {
        remoteEntry: `${window.origin}/scr-discovery/remoteEntry.js`,
        remoteName: 'scr',
        exposedModule: './Module',

        // For Routing
        displayName: 'projects.iqars.nav.sidenavmenu.scr.title',
        routePath: 'scr',
        groupRoute: '',
        ngModuleName: 'ScrModule',
        isGroup: true,
        iconClass: 'fab fa-algolia',
        groupName: 'Stammdaten',
        flatComponents: true,
        groupNameTranslationString: 'projects.iqars.nav.sidenavmenu.scr.title',
        requireRole: null,
        components: [
          {
            displayName: 'projects.iqars.nav.sidenavmenu.scr.order',
            routePath: 'scr/order',
            requireRole: null
          },
          {
            displayName: 'projects.iqars.nav.sidenavmenu.scr.relation',
            routePath: 'scr/relation',
            requireRole: null
          }
        ]
      },
      // Basedata
      {
        // For Loading
        // remoteEntry: 'http://iqarsfrontend-basedata.dev.iqars.de:8080/remoteEntry.js',
        remoteEntry: `${window.origin}/basedata-discovery/remoteEntry.js`,
        remoteName: 'basedata',
        exposedModule: './Module',

        // For Routing
        displayName: 'projects.basedata.sidenav-group-title',
        routePath: 'basedata',
        isGroup: true,
        flatComponents: true,
        groupRoute: '',
        ngModuleName: 'BaseDataModule',
        iconClass: 'fas fa-database',
        groupName: 'Basis Daten',
        groupNameTranslationString: 'projects.basedata.sidenav-group-title',
        requireRole: null,
        components: [
          {
            displayName: 'projects.basedata.components.role.title-nav',
            routePath: 'basedata/roles',
            requireRole: null
          },
          {
            displayName: 'projects.basedata.components.function.title-nav',
            routePath: 'basedata/functions',
            requireRole: null
          },
          {
            displayName: 'projects.basedata.components.unitofmeasure.title-nav',
            routePath: 'basedata/unitofmeasures',
            requireRole: null
          },
          {
            displayName: 'projects.basedata.components.country.title-nav',
            routePath: 'basedata/countries',
            requireRole: null
          },
          {
            displayName: 'projects.basedata.components.currencies.title-nav',
            routePath: 'basedata/currencies',
            requireRole: null
          },
          {
            displayName: 'projects.basedata.components.customergroup.title-nav',
            routePath: 'basedata/customergroups',
            requireRole: null
          },
          {
            displayName: 'projects.basedata.components.deliverytype.title-nav',
            routePath: 'basedata/deliverytypes',
            requireRole: null
          },
          {
            displayName: 'projects.basedata.components.frankingcode.title-nav',
            routePath: 'basedata/frankingcode',
            requireRole: null
          },
          {
            displayName: 'projects.basedata.components.incoterms.title-nav',
            routePath: 'basedata/incoterms',
            requireRole: null
          },
          {
            displayName: 'projects.basedata.components.legalform.title-nav',
            routePath: 'basedata/legalforms',
            requireRole: null
          },
          {
            displayName: 'projects.basedata.components.materialstatus.title-nav',
            routePath: 'basedata/materialstatus',
            requireRole: null
          },
          {
            displayName: 'projects.basedata.components.materialtype.title-nav',
            routePath: 'basedata/materialtypes',
            requireRole: null
          },
          {
            displayName: 'projects.basedata.components.movementtype.title-nav',
            routePath: 'basedata/movementtypes',
            requireRole: null
          },
          {
            displayName: 'projects.basedata.components.ordercategory.title-nav',
            routePath: 'basedata/ordercategories',
            requireRole: null
          },
          {
            displayName: 'projects.basedata.components.orderstatus.title-nav',
            routePath: 'basedata/orderstatus',
            requireRole: null
          },
          {
            displayName: 'projects.basedata.components.ordertype.title-nav',
            routePath: 'basedata/ordertypes',
            requireRole: null
          },
          {
            displayName: 'projects.basedata.components.paymentcondition.title-nav',
            routePath: 'basedata/paymentconditions',
            requireRole: null
          },
          {
            displayName: 'projects.basedata.components.rejectionreason.title-nav',
            routePath: 'basedata/rejectionreasons',
            requireRole: null
          },
          {
            displayName: 'projects.basedata.components.route.title-nav',
            routePath: 'basedata/routes',
            requireRole: null
          },
          {
            displayName: 'projects.basedata.components.shipmentcondition.title-nav',
            routePath: 'basedata/shipmentconditions',
            requireRole: null
          },
          {
            displayName: 'projects.basedata.components.shipmenttype.title-nav',
            routePath: 'basedata/shipmenttypes',
            requireRole: null
          },
          {
            displayName: 'projects.basedata.components.storageclass.title-nav',
            routePath: 'basedata/storageclass',
            requireRole: null
          },
          {
            displayName: 'projects.basedata.components.storagekind.title-nav',
            routePath: 'basedata/storagekinds',
            requireRole: null
          },
          {
            displayName: 'projects.basedata.components.storagetype.title-nav',
            routePath: 'basedata/storagetypes',
            requireRole: null
          },
          {
            displayName: 'projects.basedata.components.transportkey.title-nav',
            routePath: 'basedata/transportkey',
            requireRole: null
          },
          {
            displayName: 'projects.basedata.components.usageindicator.title-nav',
            routePath: 'basedata/usageindicators',
            requireRole: null
          }
        ]
      },
      // Usermanagement
      {
        // For Loading
        // remoteEntry: 'http://iqarsfrontend-usermanager.dev.iqars.de:8080/remoteEntry.js',
        remoteEntry: `${window.origin}/usermanager-discovery/remoteEntry.js`,
        remoteName: 'usermanagement',
        exposedModule: './Module',

        // For Routing
        displayName: 'projects.user-management.sidenav-group-title',
        routePath: 'user-management',
        groupRoute: '',
        isGroup: true,
        flatComponents: true,
        ngModuleName: 'UserManagementModule',
        iconClass: 'fas fa-user',
        groupName: 'User Management',
        groupNameTranslationString: 'projects.user-management.sidenav-group-title',
        requireRole: 'UserManagement.View',
        components: [
          {
            displayName: 'projects.user-management.components.um-users.title-nav',
            routePath: 'user-management/users',
            requireRole: 'UserManagement.View'
          },
          {
            displayName: 'projects.user-management.components.um-user-groups.title-nav',
            routePath: 'user-management/groups',
            requireRole: 'UserManagement.View'
          },
          {
            displayName: 'projects.user-management.components.um-roles.title-nav',
            routePath: 'user-management/roles',
            requireRole: 'UserManagement.View'
          },
          {
            displayName: 'projects.user-management.components.um-functions.title-nav',
            routePath: 'user-management/functions',
            requireRole: 'UserManagement.View'
          }
        ]
      },
      // DevTools
      {
        // For Loading
        // remoteEntry: 'http://iqarsfrontend-devtools.dev.iqars.de:8080/remoteEntry.js',
        remoteEntry: `${window.origin}/dev-tools-discovery/remoteEntry.js`,
        remoteName: 'devtools',
        exposedModule: './Module',

        // For Routing
        displayName: 'projects.dev-tools.sidenav-group-title',
        routePath: 'dev-tools',
        groupRoute: '',
        isGroup: true,
        flatComponents: true,
        ngModuleName: 'DevtoolsModule',
        iconClass: 'fas fa-toolbox',
        groupName: 'Admin tools',
        groupNameTranslationString: 'projects.dev-tools.sidenav-group-title',
        requireRole: 'Developer',
        components: [
          {
            displayName: 'projects.dev-tools.components.log-viewer.title-nav',
            routePath: 'dev-tools/LogViewer',
            requireRole: 'Developer'
          },
          {
            displayName: 'projects.dev-tools.components.parameter-editor.title-nav',
            routePath: 'dev-tools/ParameterEditor',
            requireRole: 'Developer'
          },
          {
            displayName: 'projects.dev-tools.components.translation-uploader.title-nav',
            routePath: 'dev-tools/TranslationUploader',
            requireRole: 'Developer'
          },
          {
            displayName: 'projects.dev-tools.components.gitlab-helper-ui.title-nav',
            routePath: 'dev-tools/GitLabHelperUI',
            requireRole: 'Developer'
          },
          {
            displayName: 'ClientSideGrid',
            routePath: 'dev-tools/ClientSideGrid',
            requireRole: 'Developer'
          },
          {
            displayName: 'ServerSideGrid',
            routePath: 'dev-tools/ServerSideGrid',
            requireRole: 'Developer'
          },
          {
            displayName: 'ServerSideGroupingGrid',
            routePath: 'dev-tools/ServerSideGroupingGrid',
            requireRole: 'Developer'
          },
          {
            displayName: 'ServerSideGridWithFunction',
            routePath: 'dev-tools/ServerSideGridWithFunction',
            requireRole: 'Developer'
          }
        ]
      }
    ];
  }
}

// {
//   // For Loading
//   remoteEntry: 'http://iqarsfrontend-materialdata.dev.iqars.de:8080/remoteEntry.js',
//   remoteName: 'materialdata',
//   exposedModule: './Module',

//   // For Routing
//   displayName: 'projects.materialdata.sidenav-group-title',
//   routePath: 'materialdata',
//   ngModuleName: 'MaterialdataModule',
//   iconClass: 'fas fa-cubes',
//   groupName: 'Materialdaten',
//   components: [
//     {
//       displayName: 'projects.materialdata.components.material-master-table.title-nav',
//       routePath: 'materialmaster',
//     },
//     {
//       displayName: 'projects.materialdata.components.factory-material-table.title-nav',
//       routePath: 'factorymaterial',
//     },
//     {
//       displayName: 'projects.materialdata.components.dispo-material-table.title-nav',
//       routePath: 'dispomaterial',
//     },
//     {
//       displayName: 'projects.materialdata.components.dangerous-goods.title-nav',
//       routePath: 'dangerousgoods',
//     },
//     {
//       displayName: 'projects.materialdata.components.customs-table.title-nav',
//       routePath: 'customs',
//     },
//   ],
// },
