import { LoadRemoteModuleOptions } from '@angular-architects/module-federation';

export interface Microfrontend extends LoadRemoteModuleOptions {
  remoteEntry?: string;
  displayName: string;
  routePath: string;
  groupRoute: string;
  isGroup: boolean;
  ngModuleName: string;
  flatComponents: boolean;
  components: MicrofrontendComponents[] | [];
  iconClass?: string;
  groupName: string;
  collapsed?: boolean;
  groupNameTranslationString: string;
  requireRole: string | string[] | null;
}

export interface MicrofrontendComponents {
  remoteEntry?: string;
  displayName: string;
  groupName?: string;
  routePath: string;
  requireRole: string | string[] | null;
}
