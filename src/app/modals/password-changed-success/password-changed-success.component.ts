import { Component } from '@angular/core';
import { MdbModalRef } from 'mdb-angular-ui-kit/modal';

@Component({
  selector: 'app-password-changed-success',
  templateUrl: './password-changed-success.component.html',
  styleUrls: ['./password-changed-success.component.css']
})
export class PasswordChangedSuccessComponent {

  constructor(private modalRef: MdbModalRef<PasswordChangedSuccessComponent>) {
  }

  public close(): void {
    this.modalRef.close();
  }

}
