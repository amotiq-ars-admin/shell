import { loadRemoteModule } from '@angular-architects/module-federation';
import { Routes } from '@angular/router';
import { Microfrontend } from './app/microfrontends/microfrontend';

export const buildRoutes = (options: Microfrontend[]): Routes => {
  const lazyRoutes: Routes = options.map((o) => ({
    path: o.routePath,
    loadChildren: () => loadRemoteModule(o).then((m) => m[o.ngModuleName]),
    data: { breadcrumb: o.displayName },
  }));
  return [...lazyRoutes];
};
