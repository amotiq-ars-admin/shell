import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';
import { LicenseManager } from 'ag-grid-enterprise';
import { registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';
import localeDeExtra from '@angular/common/locales/extra/de';

if (environment.production) {
  enableProdMode();
}
LicenseManager.setLicenseKey(
  'CompanyName=amotIQ automotive GmbH,LicensedGroup=IQaRS,LicenseType=MultipleApplications,LicensedConcurrentDeveloperCount=4,LicensedProductionInstancesCount=3,AssetReference=AG-021815,ExpiryDate=2_November_2022_[v2]_MTY2NzM0NzIwMDAwMA==a988b9769a398c6e0363c1e4a0931dad');
registerLocaleData(localeDe, localeDeExtra);

platformBrowserDynamic()
  .bootstrapModule(AppModule)
  .catch((err) => console.error(err));
