FROM 192.168.50.1:6000/library/nginx:stable-alpine
RUN rm -rf /usr/share/nginx/html/*
COPY nginx/nginx.conf /etc/nginx/conf.d/default.conf
COPY nginx/nginx-template.conf /etc/nginx/conf.d/nginx-template.conf
COPY set-config.sh /home/set-config.sh
COPY /dist/shell /usr/share/nginx/html
RUN chown nobody:nogroup /home/set-config.sh
RUN chmod 777 /home/set-config.sh
# expose port 80
EXPOSE 80/tcp

# run nginx
CMD ["nginx", "-g", "daemon off;"]
