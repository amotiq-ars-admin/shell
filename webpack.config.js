const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');
const mf = require('@angular-architects/module-federation/webpack');
const path = require('path');
const share = mf.share;

const sharedMappings = new mf.SharedMappings();
sharedMappings.register(
  path.join(__dirname, 'tsconfig.json'),
  [/* mapped paths to share */]);

module.exports = {
  output: {
    uniqueName: 'shell',
    publicPath: 'auto'
  },
  optimization: {
    runtimeChunk: false
  },
  resolve: {
    alias: {
      ...sharedMappings.getAliases()
    }
  },
  plugins: [
    new ModuleFederationPlugin({

      // For remotes (please adjust)
      // name: "shell",
      // filename: "remoteEntry.js",
      // exposes: {
      //     './Component': './/src/app/app.component.ts',
      // },

      // For hosts (please adjust)
      // remotes: {
      //     "mfe1": "mfe1@http://localhost:3000/remoteEntry.js",

      // },

      shared: share({
        '@angular/core': { singleton: true, strictVersion: true, requiredVersion: 'auto' },
        '@angular/common': { singleton: true, strictVersion: true, requiredVersion: 'auto' },
        '@angular/common/http': { singleton: true, strictVersion: true, requiredVersion: 'auto' },
        '@angular/router': { singleton: true, strictVersion: true, requiredVersion: 'auto' },
        '@amotiq/amotiq-base': { singleton: true, strictVersion: false, requiredVersion: 'auto' },
        '@amotiq/auth-lib': { singleton: true, strictVersion: true, requiredVersion: 'auto' },

        // APIs
        '@amotiq/basedata-api': { singleton: true, strictVersion: false, requiredVersion: 'auto' },
        '@amotiq/businesspartner-api': { singleton: true, strictVersion: false, requiredVersion: 'auto' },
        '@amotiq/businessunit-api': { singleton: true, strictVersion: false, requiredVersion: 'auto' },
        '@amotiq/edihandler-api': { singleton: true, strictVersion: false, requiredVersion: 'auto' },
        '@amotiq/einteilung-api': { singleton: true, strictVersion: false, requiredVersion: 'auto' },
        '@amotiq/export-api': { singleton: true, strictVersion: false, requiredVersion: 'auto' },
        '@amotiq/materialbasedata-api': { singleton: true, strictVersion: false, requiredVersion: 'auto' },
        '@amotiq/materialmovement-api': { singleton: true, strictVersion: false, requiredVersion: 'auto' },
        '@amotiq/mrp-api': { singleton: true, strictVersion: false, requiredVersion: 'auto' },
        '@amotiq/screlation-api': { singleton: true, strictVersion: false, requiredVersion: 'auto' },
        '@amotiq/shipmentanddelivery-api': { singleton: true, strictVersion: false, requiredVersion: 'auto' },
        '@amotiq/storagelocation-api': { singleton: true, strictVersion: false, requiredVersion: 'auto' },
        '@amotiq/weservice-api': { singleton: true, strictVersion: false, requiredVersion: 'auto' },

        'mdb-angular-ui-kit': { singleton: false, strictVersion: true, requiredVersion: 'auto' },
        '@ngx-translate/core': { singleton: true, strictVersion: true, requiredVersion: 'auto' },
        'ngx-logger': { singleton: false, strictVersion: true, requiredVersion: 'auto' },

        ...sharedMappings.getDescriptors()
      })

    }),
    sharedMappings.getPlugin()
  ]
};
