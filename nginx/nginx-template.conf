# Template config
# Variables replaced by sed during build

server {

  listen 80;

  sendfile on;

  default_type application/octet-stream;

  client_max_body_size 2G;
  client_body_buffer_size 1M;

  gzip on;
  gzip_http_version 1.1;
  gzip_disable      "MSIE [1-6]\.";
  gzip_min_length   1100;
  gzip_vary         on;
  gzip_proxied      expired no-cache no-store private auth;
  gzip_types        text/plain text/css application/json application/javascript application/x-javascript text/xml application/xml application/xml+rss text/javascript;
  gzip_comp_level   9;

  root /usr/share/nginx/html;

  resolver kube-dns.kube-system;

  # ---------------------------------------------------------------------------
  # BusinessUnitService
  # ---------------------------------------------------------------------------
  location /api/BusinessUnit/ {
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

    # don't cache it
    proxy_no_cache 1;
    # even if cached, don't try to use it
    proxy_cache_bypass 1;

    proxy_pass http://businessunitservice.${KUBE_NAMESPACE}.svc.cluster.local$request_uri;
  }

  # ---------------------------------------------------------------------------
  # StorageLocation
  # ---------------------------------------------------------------------------
  location /api/StorageLocation/ {
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

    # don't cache it
    proxy_no_cache 1;
    # even if cached, don't try to use it
    proxy_cache_bypass 1;

    proxy_pass http://storagelocationservice.${KUBE_NAMESPACE}.svc.cluster.local$request_uri;
  }

  # ---------------------------------------------------------------------------
  # Delivery
  # ---------------------------------------------------------------------------
  location /api/DeliveryService/ {
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

    # don't cache it
    proxy_no_cache 1;
    # even if cached, don't try to use it
    proxy_cache_bypass 1;

    proxy_pass http://shipmentanddeliveryservice.${KUBE_NAMESPACE}.svc.cluster.local$request_uri;
  }

  # ---------------------------------------------------------------------------
  # FactoryCalendarService
  # ---------------------------------------------------------------------------
  location /api/FactoryCalendarService/ {
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

    proxy_pass http://factorycalendarservice.${KUBE_NAMESPACE}.svc.cluster.local$request_uri;
  }

  # ---------------------------------------------------------------------------
  # BusinesspartnerService
  # ---------------------------------------------------------------------------
  location /api/BusinessPartnerService/ {
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

    # don't cache it
    proxy_no_cache 1;
    # even if cached, don't try to use it
    proxy_cache_bypass 1;

    proxy_pass http://businesspartnerservice.${KUBE_NAMESPACE}.svc.cluster.local$request_uri;
  }

  # ---------------------------------------------------------------------------
  # BaseDataService
  # ---------------------------------------------------------------------------
  location /api/BaseData/ {
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

    proxy_pass http://basedataservice.${KUBE_NAMESPACE}.svc.cluster.local$request_uri;
  }

  # ---------------------------------------------------------------------------
  # LogHandler
  # ---------------------------------------------------------------------------
  location /api/LogHandler/ {
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

    proxy_pass http://loghandler.${KUBE_NAMESPACE}.svc.cluster.local$request_uri;
  }

  # ---------------------------------------------------------------------------
  # ConfigProvider
  # ---------------------------------------------------------------------------
  location /api/ConfigProvider/ {
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

    proxy_pass http://configprovider.${KUBE_NAMESPACE}.svc.cluster.local$request_uri;
  }

  # ---------------------------------------------------------------------------
  # MaterialBaseDataService
  # ---------------------------------------------------------------------------
  location /api/MaterialBaseData/ {
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

    proxy_pass http://materialbasedataservice.${KUBE_NAMESPACE}.svc.cluster.local$request_uri;
  }

  # ---------------------------------------------------------------------------
  # MaterialBaseDataService
  # ---------------------------------------------------------------------------
  location /api/MaterialMovementJournal/ {
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

    proxy_pass http://materialmovementjournal.${KUBE_NAMESPACE}.svc.cluster.local$request_uri;
  }

   # ---------------------------------------------------------------------------
   # MRP Process
   # ---------------------------------------------------------------------------
   location /api/Mrp/ {
     proxy_set_header X-Real-IP $remote_addr;
     proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

     proxy_pass http://mrpservice.${KUBE_NAMESPACE}.svc.cluster.local$request_uri;
   }

  # ---------------------------------------------------------------------------
  # SCRelation
  # ---------------------------------------------------------------------------
  location /api/SCRelationService/ {
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

    proxy_pass http://screlationservice.${KUBE_NAMESPACE}.svc.cluster.local$request_uri;
  }

  # ---------------------------------------------------------------------------
  # Usermanager
  # ---------------------------------------------------------------------------
  location /api/UserManager/ {
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

    proxy_pass http://usermanager.oauth.svc.cluster.local$request_uri;
  }

  # ---------------------------------------------------------------------------
  # Einteilungsservice
  # ---------------------------------------------------------------------------
  location /api/EinteilungsService/ {
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

    # don't cache it
    proxy_no_cache 1;
    # even if cached, don't try to use it
    proxy_cache_bypass 1;

    proxy_pass http://einteilungsservice.${KUBE_NAMESPACE}.svc.cluster.local$request_uri;
  }

  # ---------------------------------------------------------------------------
  # LangProvider
  # ---------------------------------------------------------------------------
  location /api/LanguageProvider/ {
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

    # don't cache it
    proxy_no_cache 1;
    # even if cached, don't try to use it
    proxy_cache_bypass 1;

    proxy_pass http://langprovider.${KUBE_NAMESPACE}.svc.cluster.local$request_uri;
  }

  # ---------------------------------------------------------------------------
  # WeService
  # ---------------------------------------------------------------------------
  location /api/WeService/ {
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

    # don't cache it
    proxy_no_cache 1;
    # even if cached, don't try to use it
    proxy_cache_bypass 1;

    proxy_pass http://weservice.${KUBE_NAMESPACE}.svc.cluster.local$request_uri;
  }

  # ---------------------------------------------------------------------------
  # ExportService
  # ---------------------------------------------------------------------------
  location /api/ExportService/ {
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

    # don't cache it
    proxy_no_cache 1;
    # even if cached, don't try to use it
    proxy_cache_bypass 1;

    proxy_pass http://exportservice.${KUBE_NAMESPACE}.svc.cluster.local$request_uri;
  }

  # ---------------------------------------------------------------------------
  # GitLabHelper
  # ---------------------------------------------------------------------------
  location /api/GitLabHelper/ {
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

    # don't cache it
    proxy_no_cache 1;
    # even if cached, don't try to use it
    proxy_cache_bypass 1;

    proxy_pass http://gitlabhelper.gitlabhelper.svc.cluster.local$request_uri;
  }

  # ---------------------------------------------------------------------------
  # SignalRHub
  # ---------------------------------------------------------------------------
  location /api/SignalRHub/ {
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

    proxy_pass http://signalrhub.${KUBE_NAMESPACE}.svc.cluster.local$request_uri;
  }

  location /GuiSignal {
    proxy_pass http://signalrhub.${KUBE_NAMESPACE}.svc.cluster.local$request_uri;

    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "Upgrade";
    proxy_set_header Host $host;
  }

  location /IQaRSFrontend {
    proxy_pass http://signalrhub.${KUBE_NAMESPACE}.svc.cluster.local$request_uri;

    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "Upgrade";
    proxy_set_header Host $host;
  }

  location / {
    try_files $uri $uri/ /index.html;
  }

}
